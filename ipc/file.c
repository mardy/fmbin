#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "ctl_server.h"
#include "fmcommon.h"
#include "ipc/file.h"

typedef struct {
	FILE *req_fd; // fd to read requests from
	FILE *resp_fd; // fd to write responses to
	FILE *inter_fd; // fd to write interruption info to
} file_context_t;

static int file_init(const server_config_t *config, void **ctx)
{
	print_dbg("Initializing file-based IPC");

	file_context_t *c = malloc(sizeof(file_context_t));
	if (!c) {
		print_err("Failed to allocate context memory");
		return -3;
	}

	memset(c, 0, sizeof(file_context_t));
	*ctx = c; // will be passed to other file functions

	// Open files
	c->req_fd = fopen(config->request_file, "r");
	if (!c->req_fd) {
		print_err("Failed to open %s: %d", config->request_file, errno);
		return -1;
	}
	c->resp_fd = fopen(config->response_file, "a");
	if (!c->resp_fd) {
		print_err("Failed to open %s: %d", config->response_file, errno);
		return -1;
	}
	c->inter_fd = fopen(config->interruption_file, "a");
	if (!c->inter_fd) {
		print_err("Failed to open %s: %d", config->interruption_file, errno);
		return -1;
	}

	return 0;
}

static ssize_t file_read_request(void *ctx, void *buffer, size_t count)
{
	file_context_t *c = ctx;
	char *buf = buffer;

	if (!fgets(buf, count, c->req_fd)) {
		return -1;
	}

	size_t length = strlen(buf);
	// Overwrite the \n with a NULL terminator
	buf[length - 1] = '\0';
	return length - 1;
}

static ssize_t file_send_response(void *ctx, const void *buffer, size_t count)
{
	file_context_t *c = ctx;
	size_t ret = fwrite(buffer, 1, count, c->resp_fd);
	fputc('\n', c->resp_fd);
	return ret;
}

static ssize_t file_send_interruption(void *ctx, const void *buffer, size_t count)
{
	file_context_t *c = ctx;
	size_t ret = fwrite(buffer, 1, count, c->inter_fd);
	fputc('\n', c->inter_fd);
	return ret;
}

static int file_close(void *ctx)
{
	file_context_t *c = ctx;
	// Close file
	fclose(c->req_fd);
	fclose(c->resp_fd);
	fclose(c->inter_fd);
	free(c);
	return 0;
}

static const ipc_functions_t file_functions = {
	.init = file_init,
	.read_request = file_read_request,
	.send_response = file_send_response,
	.send_interruption = file_send_interruption,
	.close = file_close,
};

const ipc_functions_t *ipc_file_functions() {
    return &file_functions;
}
